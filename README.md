# ブレイクポイントを調べる

## 準備

```sh
$ npm install
$ npx tsc
```

## 実行

### 1. CSS をダウンロードする

入力は行区切りの URL のリスト

```sh
$ cat targets.txt
https://example.com/
https://text.com/
https://hoge.com/
```

実行

```sh
$ cat targets.txt | node dist/main.js
```

`bukkonuki/`の下に`ホスト名/タイムスタンプ`の名前でディレクトリを作り、その下に CSS をダウンロードする

### 2. メディアクエリを抽出する

- サイトごとに使われているメディアクエリを抽出する
- [rg](https://github.com/BurntSushi/ripgrep)を使っているので別途インストール

```sh
$ bash summarize.sh
./bukkonuki/example_com
(max-width: 1060px)
(max-width: 767px)
(max-width: 800px)
(min-width: 768px)
./bukkonuki/newstories_jp
(max-width: 1194px)
(max-width: 960px)
(min-width: 1194px)
(min-width: 1600px)
(min-width: 960px)
...
...
```
