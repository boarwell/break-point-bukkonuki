import puppeteer = require('puppeteer');
import prettier = require('prettier');
import { get } from 'got';

import * as fs from 'fs';
import * as path from 'path';
import { createInterface } from 'readline';

const cssDir = path.join('.', 'bukkonuki');

/**
 * 取得したCSSファイルを保存するためのディレクトリを作る
 * ディスクに保存するときのために、最終的な保存先を返す
 */
const createDirs = (url: string): string => {
  const domainDir = path.join(
    cssDir,
    new URL(url).hostname.replace(/\./g, '_')
  );

  if (!fs.existsSync(domainDir)) {
    fs.mkdirSync(domainDir);
  }

  const fileDir = path.join(domainDir, Date.now().toFixed());
  fs.mkdirSync(fileDir);

  return fileDir;
};

/**
 * CSSを文字列として受け取ってフォーマットして返す
 * もしエラーが発生するようなら入力をそのまま返す
 */
const formatCSS = (css: string): string => {
  try {
    return prettier.format(css, { parser: 'css' });
  } catch (e) {
    return css;
  }
};

const main = async (targets: string[]) => {
  if (!fs.existsSync(cssDir)) {
    fs.mkdirSync(cssDir);
  }

  const browser = await puppeteer.launch({ timeout: 5000, headless: false });

  for (const url of targets) {
    console.log(url);

    const page = await browser.newPage();
    page.setViewport({ width: 1600, height: 900 });

    const fileDir = createDirs(url);

    await page.goto(url, { waitUntil: 'domcontentloaded' }).catch(async e => {
      console.error(page.url(), e);
      await page.close();
    });

    const cssURLs = await page.$$eval(
      'link[rel="stylesheet"]',
      (links): string[] => {
        return (links as HTMLLinkElement[]).map(link => link.href);
      }
    );

    for (const cssURL of cssURLs) {
      const { body } = await get(cssURL);

      await fs.promises.writeFile(
        path.join(fileDir, path.basename(new URL(cssURL).pathname)),
        formatCSS(body)
      );
    }

    // インラインスタイルを抜く
    const inlineStyle = await page.evaluate(
      (): string => {
        const [...styleElements] = document.querySelectorAll('style');

        return styleElements.reduce(
          (prev, curr) => prev + (curr.textContent ? curr.textContent : ''),
          ''
        );
      }
    );

    if (inlineStyle !== '') {
      await fs.promises.writeFile(
        path.join(fileDir, '_inline.css'),
        formatCSS(inlineStyle)
      );
    }

    await page.close();
  }

  await browser.close();
};

const rl = createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

let buf = '';
rl.on('line', line => (buf += line + '\n'));
rl.on('close', () => main(buf.split('\n')));
