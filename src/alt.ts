import puppeteer = require('puppeteer');
import prettier = require('prettier');
import { get } from 'got';

import * as fs from 'fs';
import * as path from 'path';
import { createInterface } from 'readline';

const cssDir = path.join('.', 'bukkonuki');

/**
 * 取得したCSSファイルを保存するためのディレクトリを作る
 * ディスクに保存するときのために、最終的な保存先を返す
 */
const createDirs = (url: string): string => {
  const domainDir = path.join(
    cssDir,
    new URL(url).hostname.replace(/\./g, '_')
  );

  if (!fs.existsSync(domainDir)) {
    fs.mkdirSync(domainDir);
  }

  const fileDir = path.join(domainDir, Date.now().toFixed());
  fs.mkdirSync(fileDir);

  return fileDir;
};

/**
 * CSSを文字列として受け取ってフォーマットして返す
 * もしエラーが発生するようなら入力をそのまま返す
 */
const formatCSS = (css: string): string => {
  try {
    return prettier.format(css, { parser: 'css' });
  } catch (e) {
    return css;
  }
};

const onBrowser = (browser: puppeteer.Browser) => async (urls: string[]) => {
  const page = await browser.newPage();

  for (const url of urls) {
    const fileDir = createDirs(url);

    try {
      await page.goto(url, { waitUntil: 'networkidle2' });
    } catch (e) {
      console.error(url, e);
      continue;
    }

    const cssURLs = await page.$$eval(
      'link[rel="stylesheet"]',
      (links): string[] => {
        return (links as HTMLLinkElement[]).map(link => link.href);
      }
    );

    for (const cssURL of cssURLs) {
      try {
        const { body } = await get(cssURL);

        await fs.promises.writeFile(
          path.join(fileDir, path.basename(new URL(cssURL).pathname)),
          formatCSS(body)
        );
      } catch (e) {
        console.error(cssURL, e);
        continue;
      }
    }

    // インラインスタイルを抜く
    const inlineStyle = await page.$$eval(
      'style',
      (styles): string => {
        return (styles as HTMLStyleElement[])
          .map(style => style.textContent)
          .filter(Boolean)
          .join('\n');
      }
    );

    if (inlineStyle !== '') {
      fs.writeFileSync(
        path.join(fileDir, '_inline.css'),
        formatCSS(inlineStyle)
      );
    }
  }
};

const main = async (targets: string[]) => {
  if (!fs.existsSync(cssDir)) {
    fs.mkdirSync(cssDir);
  }

  const browser = await puppeteer.launch({ args: [' --no-sandbox'] });
  const job = await onBrowser(browser);
  await job(targets);
  await browser.close();
};

const rl = createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

let buf = '';
rl.on('line', line => (buf += line + '\n'));
rl.on('close', () => main(buf.split('\n').filter(Boolean)));
