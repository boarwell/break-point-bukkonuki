#!/usr/bin/env bash

# 各サイトごとに使われているメディアクエリを抽出する
find ./bukkonuki/* -maxdepth 0 -type d | xargs -i bash -c 'echo {}; rg -e "@media" -N {} | rg -e "\((max|min)-width:.*?\)" -N -o | sort | uniq'
